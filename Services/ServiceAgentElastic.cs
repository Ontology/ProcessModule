﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessModule.Services
{
    public class ServiceAgentElastic : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;
        private object serviceLocker = new object();

        private ServiceResult resultProcessTree;
        public ServiceResult ResultProcessTree
        {
            get
            {
                lock(serviceLocker)
                {
                    return resultProcessTree;
                }
            }
            set
            {
                lock(serviceLocker)
                {
                    resultProcessTree = value;
                }
                RaisePropertyChanged(nameof(ResultProcessTree));
            }
        }

        public async Task<ServiceResult> GetProcessTree()
        {
            var serviceResult = new ServiceResult
            {
                Result = localConfig.Globals.LState_Success.Clone()
            };

            var dbReaderPublic = new OntologyModDBConnector(localConfig.Globals);
            var dbReaderSub = new OntologyModDBConnector(localConfig.Globals);

            var search = new List<clsObjectAtt>
            {
                new clsObjectAtt
                {
                    ID_AttributeType = localConfig.OItem_attribute_public.GUID,
                    ID_Class = localConfig.OItem_type_process.GUID,
                    Val_Bit = true
                }
            };

            serviceResult.Result = dbReaderPublic.GetDataObjectAtt(search);

            if (serviceResult.Result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                serviceResult.PublicProcesses = dbReaderPublic.ObjAtts;
                serviceResult.Result = dbReaderSub.GetDataObjectsTree(localConfig.OItem_type_process, localConfig.OItem_type_process, localConfig.OItem_relationtype_superordinate);

                if (serviceResult.Result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    serviceResult.ProcessTree = dbReaderSub.ObjectTree;
                }
            }

            ResultProcessTree = serviceResult;
            return serviceResult;
        }

        public clsOntologyItem GetOItem(string id, string type)
        {
            var dbReader = new OntologyModDBConnector(localConfig.Globals);
            return dbReader.GetOItem(id, type);

        }

        public ServiceAgentElastic(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
        }
    }


    public class ServiceResult
    {
        public clsOntologyItem Result { get; set; }
        public List<clsObjectAtt> PublicProcesses { get; set; }
        public List<clsObjectTree> ProcessTree { get; set; }
    }
}
