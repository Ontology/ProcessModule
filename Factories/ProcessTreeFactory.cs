﻿using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using ProcessModule.Models;
using ProcessModule.Nofications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessModule.Factories
{
    public class ProcessTreeFactory : NotifyPropertyChange
    {

        private clsLocalConfig localConfig;

        private object factoryLocker = new object();

        public ResultProcessTree resultProcessTree;
        public ResultProcessTree ResultProcessTree
        {
            get
            {
                lock(factoryLocker)
                {
                    return resultProcessTree;
                }
            }
            set
            {
                lock(factoryLocker)
                {
                    resultProcessTree = value;
                    
                }
                RaisePropertyChanged(nameof(ResultProcessTree));
            }
        }

        public async Task<ResultProcessTree> GetAndWriteTreeListNodes(List<clsObjectAtt> publicProcesses, List<clsObjectTree> objectTree, SessionFile sessionFile)
        {
            var resultItem = new ResultProcessTree
            {
                Result = localConfig.Globals.LState_Success.Clone(),
                TreeNodes = new List<TreeListNode>(),
                SessionFile = sessionFile
            };

            publicProcesses.OrderBy(pubProc => pubProc.Name_Object).ToList().ForEach(pubProc =>
            {
                var treeNode = new TreeListNode
                {
                    IdNode = pubProc.ID_Object,
                    NameNode = pubProc.Name_Object,
                    PathNode = pubProc.Name_Object,
                    IconClass = IconClasses.Process
                };

                resultItem.TreeNodes.Add(treeNode);

                AddSubNodes(objectTree, resultItem, treeNode);
            });

            resultItem.Result = WriteTreeListJson(resultItem);

            ResultProcessTree = resultItem;
            return resultItem;
        }

        private clsOntologyItem WriteTreeListJson(ResultProcessTree resultItem)
        {
            if (resultItem.Result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return resultItem.Result;
            }

            using (resultItem.SessionFile.StreamWriter)
            {
                using (var jsonTextWriter = new Newtonsoft.Json.JsonTextWriter(resultItem.SessionFile.StreamWriter))
                {
                    jsonTextWriter.WriteStartArray();

                    foreach (var treeNode in resultItem.TreeNodes)
                    {
                        jsonTextWriter.WriteStartObject();

                        jsonTextWriter.WritePropertyName("id");
                        jsonTextWriter.WriteValue(treeNode.IdNode);

                        jsonTextWriter.WritePropertyName(nameof(TreeListNode.NameNode));
                        jsonTextWriter.WriteValue(treeNode.NameNode);

                        
                        jsonTextWriter.WritePropertyName("parentId");
                        jsonTextWriter.WriteValue(treeNode.IdParentNode);

                        jsonTextWriter.WritePropertyName(nameof(TreeListNode.PathNode));
                        jsonTextWriter.WriteValue(treeNode.PathNode);

                        jsonTextWriter.WritePropertyName(nameof(TreeListNode.IconClass));
                        jsonTextWriter.WriteValue(treeNode.IconClass);

                        jsonTextWriter.WriteEndObject();
                    }

                    jsonTextWriter.WriteEndArray();
                }
            }

            return resultItem.Result;
        }

        private void AddSubNodes(List<clsObjectTree> objectTree, ResultProcessTree resultItem, TreeListNode parentNode)
        {
            var subProcs = objectTree.Where(objNode => objNode.ID_Object_Parent == parentNode.IdNode).OrderBy(objNode => objNode.OrderID).ThenBy(objNode => objNode.Name_Object).ToList();

            subProcs.ForEach(subProc =>
            {
                var treeNode = new TreeListNode
                {
                    IdNode = subProc.ID_Object,
                    NameNode = subProc.Name_Object,
                    IdParentNode = parentNode.IdNode,
                    PathNode = parentNode.PathNode + @"\" + subProc.Name_Object,
                    IconClass = IconClasses.Process
                };

                resultItem.TreeNodes.Add(treeNode);

                AddSubNodes(objectTree, resultItem, treeNode);
            });
        }

        public ProcessTreeFactory(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
        }
    }

    public class ResultProcessTree
    {
        public clsOntologyItem Result { get; set; }
        public List<TreeListNode> TreeNodes { get; set; }
        public SessionFile SessionFile { get; set; }
    }
}
