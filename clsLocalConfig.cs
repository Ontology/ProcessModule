﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OntologyAppDBConnector;
using ImportExport_Module;
using OntologyClasses.BaseClasses;
using System.Reflection;
using OntologyClasses.Interfaces;
using System.Runtime.InteropServices;

namespace ProcessModule
{
    public class clsLocalConfig : ILocalConfig
    {
        private const string cstrID_Ontology = "92612f22c20f4e4e97020e7f6016e5b8";
        private ImportWorker objImport;

        public Globals Globals { get; set; }

        private clsOntologyItem objOItem_DevConfig = new clsOntologyItem();
        public clsOntologyItem OItem_BaseConfig { get; set; }

        private OntologyModDBConnector objDBLevel_Config1;
        private OntologyModDBConnector objDBLevel_Config2;

        public clsOntologyItem OItem_type_file { get; set; }
        public clsOntologyItem OItem_type_process_references { get; set; }
        public clsOntologyItem OItem_relationtype_contains { get; set; }
        public clsOntologyItem OItem_relationtype_isdescribedby { get; set; }
        public clsOntologyItem OItem_attribute_value { get; set; }
        public clsOntologyItem OItem_relationtype_needs_child { get; set; }
        public clsOntologyItem OItem_type_group { get; set; }
        public clsOntologyItem OItem_type_process_module { get; set; }
        public clsOntologyItem OItem_attribute_description { get; set; }
        public clsOntologyItem OItem_relationtype_offered_by { get; set; }
        public clsOntologyItem OItem_relationtype_belonging_material { get; set; }
        public clsOntologyItem OItem_relationtype_belongsto { get; set; }
        public clsOntologyItem OItem_relationtype_belonging_sem_item { get; set; }
        public clsOntologyItem OItem_type_manual { get; set; }
        public clsOntologyItem OItem_attribute_dbpostfix { get; set; }
        public clsOntologyItem OItem_object_process_module { get; set; }
        public clsOntologyItem OItem_type_module { get; set; }
        public clsOntologyItem OItem_attribute_public { get; set; }
        public clsOntologyItem OItem_type_variable { get; set; }
        public clsOntologyItem OItem_type_media { get; set; }
        public clsOntologyItem OItem_type_language { get; set; }
        public clsOntologyItem OItem_relationtype_needed_documentation { get; set; }
        public clsOntologyItem OItem_relationtype_todo_for { get; set; }
        public clsOntologyItem OItem_type_role { get; set; }
        public clsOntologyItem OItem_type_process { get; set; }
        public clsOntologyItem OItem_type_user { get; set; }
        public clsOntologyItem OItem_type_responsibility { get; set; }
        public clsOntologyItem OItem_type_application { get; set; }
        public clsOntologyItem OItem_relationtype_superordinate { get; set; }
        public clsOntologyItem OItem_relationtype_needs { get; set; }
        public clsOntologyItem OItem_relationtype_needs_authority { get; set; }
        public clsOntologyItem OItem_attribute_count { get; set; }
        public clsOntologyItem OItem_relationtype_belonging_util { get; set; }
        public clsOntologyItem OItem_type_things_references { get; set; }
        public clsOntologyItem OItem_type_folder { get; set; }



        private void get_Data_DevelopmentConfig()
        {
            var objORL_Ontology_To_OntolgyItems = new List<clsObjectRel> {new clsObjectRel {ID_Object = cstrID_Ontology,
                                                                                             ID_RelationType = Globals.RelationType_contains.GUID,
                                                                                             ID_Parent_Other = Globals.Class_OntologyItems.GUID}};

            var objOItem_Result = objDBLevel_Config1.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
            if (objOItem_Result.GUID == Globals.LState_Success.GUID)
            {
                if (objDBLevel_Config1.ObjectRels.Any())
                {

                    objORL_Ontology_To_OntolgyItems = objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                    {
                        ID_Object = oi.ID_Other,
                        ID_RelationType = Globals.RelationType_belongingAttribute.GUID
                    }).ToList();

                    objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                    {
                        ID_Object = oi.ID_Other,
                        ID_RelationType = Globals.RelationType_belongingClass.GUID
                    }));
                    objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                    {
                        ID_Object = oi.ID_Other,
                        ID_RelationType = Globals.RelationType_belongingObject.GUID
                    }));
                    objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                    {
                        ID_Object = oi.ID_Other,
                        ID_RelationType = Globals.RelationType_belongingRelationType.GUID
                    }));

                    objOItem_Result = objDBLevel_Config2.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
                    if (objOItem_Result.GUID == Globals.LState_Success.GUID)
                    {
                        if (!objDBLevel_Config2.ObjectRels.Any())
                        {
                            throw new Exception("Config-Error");
                        }
                    }
                    else
                    {
                        throw new Exception("Config-Error");
                    }
                }
                else
                {
                    throw new Exception("Config-Error");
                }

            }

        }

        public clsLocalConfig()
        {
            Globals = new Globals();
            set_DBConnection();
            get_Config();
        }

        public clsLocalConfig(Globals Globals)
        {
            this.Globals = Globals;
            set_DBConnection();
            get_Config();
        }

        private void set_DBConnection()
        {
            objDBLevel_Config1 = new OntologyModDBConnector(Globals);
            objDBLevel_Config2 = new OntologyModDBConnector(Globals);
            objImport = new ImportWorker(Globals);
        }

        private void get_Config()
        {
            try
            {
                get_Data_DevelopmentConfig();
                get_Config_AttributeTypes();
                get_Config_RelationTypes();
                get_Config_Classes();
                get_Config_Objects();
            }
            catch (Exception ex)
            {
                var objAssembly = Assembly.GetExecutingAssembly();
                AssemblyTitleAttribute[] objCustomAttributes = (AssemblyTitleAttribute[])objAssembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                var strTitle = "Unbekannt";
                if (objCustomAttributes.Length == 1)
                {
                    strTitle = objCustomAttributes.First().Title;
                }

                var objOItem_Result = objImport.ImportTemplates(objAssembly);
                if (objOItem_Result.GUID != Globals.LState_Error.GUID)
                {
                    get_Data_DevelopmentConfig();
                    get_Config_AttributeTypes();
                    get_Config_RelationTypes();
                    get_Config_Classes();
                    get_Config_Objects();
                }
                else
                {
                    throw new Exception("Config not importable");
                }
                
            }
        }

        private void get_Config_AttributeTypes()
        {
            var objOList_attribute_value = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "attribute_value".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                            select objRef).ToList();

            if (objOList_attribute_value.Any())
            {
                OItem_attribute_value = new clsOntologyItem()
                {
                    GUID = objOList_attribute_value.First().ID_Other,
                    Name = objOList_attribute_value.First().Name_Other,
                    GUID_Parent = objOList_attribute_value.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attribute_description = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "attribute_description".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                  select objRef).ToList();

            if (objOList_attribute_description.Any())
            {
                OItem_attribute_description = new clsOntologyItem()
                {
                    GUID = objOList_attribute_description.First().ID_Other,
                    Name = objOList_attribute_description.First().Name_Other,
                    GUID_Parent = objOList_attribute_description.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attribute_dbpostfix = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "attribute_dbpostfix".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                select objRef).ToList();

            if (objOList_attribute_dbpostfix.Any())
            {
                OItem_attribute_dbpostfix = new clsOntologyItem()
                {
                    GUID = objOList_attribute_dbpostfix.First().ID_Other,
                    Name = objOList_attribute_dbpostfix.First().Name_Other,
                    GUID_Parent = objOList_attribute_dbpostfix.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attribute_public = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "attribute_public".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                             select objRef).ToList();

            if (objOList_attribute_public.Any())
            {
                OItem_attribute_public = new clsOntologyItem()
                {
                    GUID = objOList_attribute_public.First().ID_Other,
                    Name = objOList_attribute_public.First().Name_Other,
                    GUID_Parent = objOList_attribute_public.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attribute_count = (from objOItem in objDBLevel_Config1.ObjectRels
                                            where objOItem.ID_Object == cstrID_Ontology
                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                            where objRef.Name_Object.ToLower() == "attribute_count".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                            select objRef).ToList();

            if (objOList_attribute_count.Any())
            {
                OItem_attribute_count = new clsOntologyItem()
                {
                    GUID = objOList_attribute_count.First().ID_Other,
                    Name = objOList_attribute_count.First().Name_Other,
                    GUID_Parent = objOList_attribute_count.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }


        }

        private void get_Config_RelationTypes()
        {
            var objOList_relationtype_contains = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "relationtype_contains".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                  select objRef).ToList();

            if (objOList_relationtype_contains.Any())
            {
                OItem_relationtype_contains = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_contains.First().ID_Other,
                    Name = objOList_relationtype_contains.First().Name_Other,
                    GUID_Parent = objOList_relationtype_contains.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_isdescribedby = (from objOItem in objDBLevel_Config1.ObjectRels
                                                       where objOItem.ID_Object == cstrID_Ontology
                                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                       where objRef.Name_Object.ToLower() == "relationtype_isdescribedby".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                       select objRef).ToList();

            if (objOList_relationtype_isdescribedby.Any())
            {
                OItem_relationtype_isdescribedby = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_isdescribedby.First().ID_Other,
                    Name = objOList_relationtype_isdescribedby.First().Name_Other,
                    GUID_Parent = objOList_relationtype_isdescribedby.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_needs_child = (from objOItem in objDBLevel_Config1.ObjectRels
                                                     where objOItem.ID_Object == cstrID_Ontology
                                                     join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                     where objRef.Name_Object.ToLower() == "relationtype_needs_child".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                     select objRef).ToList();

            if (objOList_relationtype_needs_child.Any())
            {
                OItem_relationtype_needs_child = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_needs_child.First().ID_Other,
                    Name = objOList_relationtype_needs_child.First().Name_Other,
                    GUID_Parent = objOList_relationtype_needs_child.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_offered_by = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_offered_by".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

            if (objOList_relationtype_offered_by.Any())
            {
                OItem_relationtype_offered_by = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_offered_by.First().ID_Other,
                    Name = objOList_relationtype_offered_by.First().Name_Other,
                    GUID_Parent = objOList_relationtype_offered_by.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_belonging_material = (from objOItem in objDBLevel_Config1.ObjectRels
                                                            where objOItem.ID_Object == cstrID_Ontology
                                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                            where objRef.Name_Object.ToLower() == "relationtype_belonging_material".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                            select objRef).ToList();

            if (objOList_relationtype_belonging_material.Any())
            {
                OItem_relationtype_belonging_material = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_belonging_material.First().ID_Other,
                    Name = objOList_relationtype_belonging_material.First().Name_Other,
                    GUID_Parent = objOList_relationtype_belonging_material.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_belongsto = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "relationtype_belongsto".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                   select objRef).ToList();

            if (objOList_relationtype_belongsto.Any())
            {
                OItem_relationtype_belongsto = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_belongsto.First().ID_Other,
                    Name = objOList_relationtype_belongsto.First().Name_Other,
                    GUID_Parent = objOList_relationtype_belongsto.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_belonging_sem_item = (from objOItem in objDBLevel_Config1.ObjectRels
                                                            where objOItem.ID_Object == cstrID_Ontology
                                                            join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                            where objRef.Name_Object.ToLower() == "relationtype_belonging_sem_item".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                            select objRef).ToList();

            if (objOList_relationtype_belonging_sem_item.Any())
            {
                OItem_relationtype_belonging_sem_item = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_belonging_sem_item.First().ID_Other,
                    Name = objOList_relationtype_belonging_sem_item.First().Name_Other,
                    GUID_Parent = objOList_relationtype_belonging_sem_item.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_needed_documentation = (from objOItem in objDBLevel_Config1.ObjectRels
                                                              where objOItem.ID_Object == cstrID_Ontology
                                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                              where objRef.Name_Object.ToLower() == "relationtype_needed_documentation".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                              select objRef).ToList();

            if (objOList_relationtype_needed_documentation.Any())
            {
                OItem_relationtype_needed_documentation = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_needed_documentation.First().ID_Other,
                    Name = objOList_relationtype_needed_documentation.First().Name_Other,
                    GUID_Parent = objOList_relationtype_needed_documentation.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_todo_for = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "relationtype_todo_for".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                  select objRef).ToList();

            if (objOList_relationtype_todo_for.Any())
            {
                OItem_relationtype_todo_for = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_todo_for.First().ID_Other,
                    Name = objOList_relationtype_todo_for.First().Name_Other,
                    GUID_Parent = objOList_relationtype_todo_for.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_superordinate = (from objOItem in objDBLevel_Config1.ObjectRels
                                                       where objOItem.ID_Object == cstrID_Ontology
                                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                       where objRef.Name_Object.ToLower() == "relationtype_superordinate".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                       select objRef).ToList();

            if (objOList_relationtype_superordinate.Any())
            {
                OItem_relationtype_superordinate = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_superordinate.First().ID_Other,
                    Name = objOList_relationtype_superordinate.First().Name_Other,
                    GUID_Parent = objOList_relationtype_superordinate.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_needs = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "relationtype_needs".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                               select objRef).ToList();

            if (objOList_relationtype_needs.Any())
            {
                OItem_relationtype_needs = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_needs.First().ID_Other,
                    Name = objOList_relationtype_needs.First().Name_Other,
                    GUID_Parent = objOList_relationtype_needs.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_needs_authority = (from objOItem in objDBLevel_Config1.ObjectRels
                                                         where objOItem.ID_Object == cstrID_Ontology
                                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                         where objRef.Name_Object.ToLower() == "relationtype_needs_authority".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                         select objRef).ToList();

            if (objOList_relationtype_needs_authority.Any())
            {
                OItem_relationtype_needs_authority = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_needs_authority.First().ID_Other,
                    Name = objOList_relationtype_needs_authority.First().Name_Other,
                    GUID_Parent = objOList_relationtype_needs_authority.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_belonging_util = (from objOItem in objDBLevel_Config1.ObjectRels
                                                        where objOItem.ID_Object == cstrID_Ontology
                                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                        where objRef.Name_Object.ToLower() == "relationtype_belonging_util".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                        select objRef).ToList();

            if (objOList_relationtype_belonging_util.Any())
            {
                OItem_relationtype_belonging_util = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_belonging_util.First().ID_Other,
                    Name = objOList_relationtype_belonging_util.First().Name_Other,
                    GUID_Parent = objOList_relationtype_belonging_util.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }


        }

        private void get_Config_Objects()
        {
            var objOList_object_process_module = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "object_process_module".ToLower() && objRef.Ontology == Globals.Type_Object
                                                  select objRef).ToList();

            if (objOList_object_process_module.Any())
            {
                OItem_object_process_module = new clsOntologyItem()
                {
                    GUID = objOList_object_process_module.First().ID_Other,
                    Name = objOList_object_process_module.First().Name_Other,
                    GUID_Parent = objOList_object_process_module.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }


        }

        private void get_Config_Classes()
        {
            var objOList_type_file = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "type_file".ToLower() && objRef.Ontology == Globals.Type_Class
                                      select objRef).ToList();

            if (objOList_type_file.Any())
            {
                OItem_type_file = new clsOntologyItem()
                {
                    GUID = objOList_type_file.First().ID_Other,
                    Name = objOList_type_file.First().Name_Other,
                    GUID_Parent = objOList_type_file.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_process_references = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "type_process_references".ToLower() && objRef.Ontology == Globals.Type_Class
                                                    select objRef).ToList();

            if (objOList_type_process_references.Any())
            {
                OItem_type_process_references = new clsOntologyItem()
                {
                    GUID = objOList_type_process_references.First().ID_Other,
                    Name = objOList_type_process_references.First().Name_Other,
                    GUID_Parent = objOList_type_process_references.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_group = (from objOItem in objDBLevel_Config1.ObjectRels
                                       where objOItem.ID_Object == cstrID_Ontology
                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                       where objRef.Name_Object.ToLower() == "type_group".ToLower() && objRef.Ontology == Globals.Type_Class
                                       select objRef).ToList();

            if (objOList_type_group.Any())
            {
                OItem_type_group = new clsOntologyItem()
                {
                    GUID = objOList_type_group.First().ID_Other,
                    Name = objOList_type_group.First().Name_Other,
                    GUID_Parent = objOList_type_group.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_process_module = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "type_process_module".ToLower() && objRef.Ontology == Globals.Type_Class
                                                select objRef).ToList();

            if (objOList_type_process_module.Any())
            {
                OItem_type_process_module = new clsOntologyItem()
                {
                    GUID = objOList_type_process_module.First().ID_Other,
                    Name = objOList_type_process_module.First().Name_Other,
                    GUID_Parent = objOList_type_process_module.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_manual = (from objOItem in objDBLevel_Config1.ObjectRels
                                        where objOItem.ID_Object == cstrID_Ontology
                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                        where objRef.Name_Object.ToLower() == "type_manual".ToLower() && objRef.Ontology == Globals.Type_Class
                                        select objRef).ToList();

            if (objOList_type_manual.Any())
            {
                OItem_type_manual = new clsOntologyItem()
                {
                    GUID = objOList_type_manual.First().ID_Other,
                    Name = objOList_type_manual.First().Name_Other,
                    GUID_Parent = objOList_type_manual.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_module = (from objOItem in objDBLevel_Config1.ObjectRels
                                        where objOItem.ID_Object == cstrID_Ontology
                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                        where objRef.Name_Object.ToLower() == "type_module".ToLower() && objRef.Ontology == Globals.Type_Class
                                        select objRef).ToList();

            if (objOList_type_module.Any())
            {
                OItem_type_module = new clsOntologyItem()
                {
                    GUID = objOList_type_module.First().ID_Other,
                    Name = objOList_type_module.First().Name_Other,
                    GUID_Parent = objOList_type_module.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_variable = (from objOItem in objDBLevel_Config1.ObjectRels
                                          where objOItem.ID_Object == cstrID_Ontology
                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                          where objRef.Name_Object.ToLower() == "type_variable".ToLower() && objRef.Ontology == Globals.Type_Class
                                          select objRef).ToList();

            if (objOList_type_variable.Any())
            {
                OItem_type_variable = new clsOntologyItem()
                {
                    GUID = objOList_type_variable.First().ID_Other,
                    Name = objOList_type_variable.First().Name_Other,
                    GUID_Parent = objOList_type_variable.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_media = (from objOItem in objDBLevel_Config1.ObjectRels
                                       where objOItem.ID_Object == cstrID_Ontology
                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                       where objRef.Name_Object.ToLower() == "type_media".ToLower() && objRef.Ontology == Globals.Type_Class
                                       select objRef).ToList();

            if (objOList_type_media.Any())
            {
                OItem_type_media = new clsOntologyItem()
                {
                    GUID = objOList_type_media.First().ID_Other,
                    Name = objOList_type_media.First().Name_Other,
                    GUID_Parent = objOList_type_media.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_language = (from objOItem in objDBLevel_Config1.ObjectRels
                                          where objOItem.ID_Object == cstrID_Ontology
                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                          where objRef.Name_Object.ToLower() == "type_language".ToLower() && objRef.Ontology == Globals.Type_Class
                                          select objRef).ToList();

            if (objOList_type_language.Any())
            {
                OItem_type_language = new clsOntologyItem()
                {
                    GUID = objOList_type_language.First().ID_Other,
                    Name = objOList_type_language.First().Name_Other,
                    GUID_Parent = objOList_type_language.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_role = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "type_role".ToLower() && objRef.Ontology == Globals.Type_Class
                                      select objRef).ToList();

            if (objOList_type_role.Any())
            {
                OItem_type_role = new clsOntologyItem()
                {
                    GUID = objOList_type_role.First().ID_Other,
                    Name = objOList_type_role.First().Name_Other,
                    GUID_Parent = objOList_type_role.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_process = (from objOItem in objDBLevel_Config1.ObjectRels
                                         where objOItem.ID_Object == cstrID_Ontology
                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                         where objRef.Name_Object.ToLower() == "type_process".ToLower() && objRef.Ontology == Globals.Type_Class
                                         select objRef).ToList();

            if (objOList_type_process.Any())
            {
                OItem_type_process = new clsOntologyItem()
                {
                    GUID = objOList_type_process.First().ID_Other,
                    Name = objOList_type_process.First().Name_Other,
                    GUID_Parent = objOList_type_process.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_user = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "type_user".ToLower() && objRef.Ontology == Globals.Type_Class
                                      select objRef).ToList();

            if (objOList_type_user.Any())
            {
                OItem_type_user = new clsOntologyItem()
                {
                    GUID = objOList_type_user.First().ID_Other,
                    Name = objOList_type_user.First().Name_Other,
                    GUID_Parent = objOList_type_user.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_responsibility = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "type_responsibility".ToLower() && objRef.Ontology == Globals.Type_Class
                                                select objRef).ToList();

            if (objOList_type_responsibility.Any())
            {
                OItem_type_responsibility = new clsOntologyItem()
                {
                    GUID = objOList_type_responsibility.First().ID_Other,
                    Name = objOList_type_responsibility.First().Name_Other,
                    GUID_Parent = objOList_type_responsibility.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_application = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "type_application".ToLower() && objRef.Ontology == Globals.Type_Class
                                             select objRef).ToList();

            if (objOList_type_application.Any())
            {
                OItem_type_application = new clsOntologyItem()
                {
                    GUID = objOList_type_application.First().ID_Other,
                    Name = objOList_type_application.First().Name_Other,
                    GUID_Parent = objOList_type_application.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_things_references = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "type_things_references".ToLower() && objRef.Ontology == Globals.Type_Class
                                                   select objRef).ToList();

            if (objOList_type_things_references.Any())
            {
                OItem_type_things_references = new clsOntologyItem()
                {
                    GUID = objOList_type_things_references.First().ID_Other,
                    Name = objOList_type_things_references.First().Name_Other,
                    GUID_Parent = objOList_type_things_references.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_type_folder = (from objOItem in objDBLevel_Config1.ObjectRels
                                        where objOItem.ID_Object == cstrID_Ontology
                                        join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                        where objRef.Name_Object.ToLower() == "type_folder".ToLower() && objRef.Ontology == Globals.Type_Class
                                        select objRef).ToList();

            if (objOList_type_folder.Any())
            {
                OItem_type_folder = new clsOntologyItem()
                {
                    GUID = objOList_type_folder.First().ID_Other,
                    Name = objOList_type_folder.First().Name_Other,
                    GUID_Parent = objOList_type_folder.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }


        }

        public string IdLocalConfig
        {
            get
            {
                var attrib =
                      Assembly.GetExecutingAssembly()
                          .GetCustomAttributes(true)
                          .FirstOrDefault(objAttribute => objAttribute is GuidAttribute);
                if (attrib != null)
                {
                    return ((GuidAttribute)attrib).Value;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
