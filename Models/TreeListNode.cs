﻿using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessModule.Models
{
    [KendoTreeListConfig(expanded = false, filterable = true, sortable = true)]
    public class TreeListNode
    {
        [KendoDataSourceField(IsIdField = true, nullable = false, type = "string")]
        public string IdNode { get; set; }

        [KendoDataSourceField(nullable = false, type = "string")]
        [KendoTreeListColumn(editable = true, hidden = false, Order = 0, title = "Process", filterable = true)]
        public string NameNode { get; set; }

        [KendoDataSourceField(nullable = false, type = "string")]
        [KendoTreeListColumn(editable = false, hidden = false, Order = 1, title = "Path", filterable = true)]
        public string PathNode { get; set; }

        [KendoDataSourceField(IsParentIdField = true, nullable = true, type = "string")]
        public string IdParentNode { get; set; }

        [KendoDataSourceField(nullable = false, type = "string")]
        public string IconClass { get; set; }

    }
}
