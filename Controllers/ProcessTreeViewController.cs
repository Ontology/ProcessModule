﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Base;
using OntoMsg_Module.Models;
using OntoMsg_Module.Notifications;
using OntoMsg_Module.WebSocketServices;
using OntoWebCore.Factories;
using ProcessModule.Factories;
using ProcessModule.Models;
using ProcessModule.Services;
using ProcessModule.Translations;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System;
using System.ComponentModel;

namespace ProcessModule.Controllers
{
    public class ProcessTreeViewController : ProcessTreeViewModel, IViewController
    {
        private WebsocketServiceAgent webSocketServiceAgent;

        private TranslationController translationController = new TranslationController();

        private ServiceAgentElastic serviceAgentElastic;

        private ProcessTreeFactory processTreeFactory;
        private TreeFactory treeFactory;


        private clsLocalConfig localConfig;

        private clsOntologyItem oItemProcess;

        public string Guid { get; private set; }

        public OntoMsg_Module.StateMachines.IControllerStateMachine StateMachine { get; private set; }
        public OntoMsg_Module.StateMachines.ControllerStateMachine LocalStateMachine
        {
            get
            {
                if (StateMachine == null)
                {
                    return null;
                }

                return (OntoMsg_Module.StateMachines.ControllerStateMachine)StateMachine;
            }
        }

        public ProcessTreeViewController()
        {
            // local configuration
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }

            Initialize();
            PropertyChanged += ProcessTreeViewController_PropertyChanged;
        }

        private void ProcessTreeViewController_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var property = ViewModelProperties.FirstOrDefault(viewProperty => viewProperty.Property.Name == e.PropertyName);

            if (property == null) return;

            property.ViewItem.AddValue(property.Property.GetValue(this));

            webSocketServiceAgent.SendPropertyChange(e.PropertyName);
        }

        private void Initialize()
        {
            var stateMachine = new OntoMsg_Module.StateMachines.ControllerStateMachine(OntoMsg_Module.StateMachines.StateMachineType.BlockSelectingSelected | OntoMsg_Module.StateMachines.StateMachineType.BlockLoadingSelected);
            stateMachine.IsControllerListen = true;
            StateMachine = stateMachine;
            stateMachine.PropertyChanged += StateMachine_PropertyChanged;
            stateMachine.loadSelectedItem += StateMachine_loadSelectedItem;
            stateMachine.loginSucceded += StateMachine_loginSucceded;
            stateMachine.openedSocket += StateMachine_openedSocket;
            stateMachine.closedSocket += StateMachine_closedSocket;

            serviceAgentElastic = new ServiceAgentElastic(localConfig);
            serviceAgentElastic.PropertyChanged += ServiceAgentElastic_PropertyChanged;
            processTreeFactory = new ProcessTreeFactory(localConfig);
            processTreeFactory.PropertyChanged += ProcessTreeFactory_PropertyChanged;

            treeFactory = new TreeFactory(localConfig.Globals);
            treeFactory.PropertyChanged += TreeFactory_PropertyChanged;

        }

        private void StateMachine_closedSocket()
        {
            webSocketServiceAgent.RemoveAllResources();
        }

        private void StateMachine_openedSocket()
        {
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                EndPointId = webSocketServiceAgent.EndpointId,
                EndpointType = EndpointType.Sender,
                SessionId = webSocketServiceAgent.DataText_SessionId
            });
        }

        private void StateMachine_loginSucceded()
        {
            
            webSocketServiceAgent.RegisterEndpoint(new ChannelEndpoint
            {
                ChannelTypeId = Channels.ParameterList,
                EndpointType = EndpointType.Receiver,
            });

            IsToggled_Listen = true;

            Text_View = translationController.Text_ViewProcessTree;

            var resultTask = serviceAgentElastic.GetProcessTree();

            
            
        }

        private void StateMachine_loadSelectedItem(clsOntologyItem oItemSelected)
        {
            
        }

        private void StateMachine_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(OntoMsg_Module.StateMachines.ControllerStateMachine.LoginSuccessful))
            {
                IsSuccessful_Login = LocalStateMachine.LoginSuccessful;
            }
        }

        private void TreeFactory_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(TreeFactory.ResultTreeListConfig))
            {
                if (treeFactory.ResultTreeListConfig.Result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    Dictionary_TreeListConfig = treeFactory.ResultTreeListConfig.TreeListConfig;
                }
            }
        }

        private void ProcessTreeFactory_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ProcessTreeFactory.ResultProcessTree))
            {
                if (processTreeFactory.ResultProcessTree.Result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var resultTask = treeFactory.CreateKendoTreeListConfig(typeof(TreeListNode), processTreeFactory.ResultProcessTree.SessionFile);
                }
            }
        }

        private void ServiceAgentElastic_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(ServiceAgentElastic.ResultProcessTree))
            {
                if (serviceAgentElastic.ResultProcessTree.Result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var sessionFile = webSocketServiceAgent.RequestWriteStream(localConfig.Globals.NewGUID + ".json");
                    var resultTask = processTreeFactory.GetAndWriteTreeListNodes(serviceAgentElastic.ResultProcessTree.PublicProcesses,
                        serviceAgentElastic.ResultProcessTree.ProcessTree,
                        sessionFile);
                }
            }
        }

        public void InitializeViewController(WebsocketServiceAgent webSocketServiceAgent)
        {
            this.webSocketServiceAgent = webSocketServiceAgent;
            this.webSocketServiceAgent.PropertyChanged += WebSocketServiceAgent_PropertyChanged;
            this.webSocketServiceAgent.comServerOnMessage += WebSocketServiceAgent_comServerOnMessage;
            this.webSocketServiceAgent.comServerOpened += WebSocketServiceAgent_comServerOpened;


        }

       

        private void WebSocketServiceAgent_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_DataText_SessionId)
            {


            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Command_RequestedCommand)
            {
                
            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_Event_RequestEvent)
            {

            }
            else if (e.PropertyName == OntoMsg_Module.Notifications.NotifyChanges.Websocket_ChangedViewItems)
            {
                webSocketServiceAgent.ChangedViewItems.ForEach(changedItem =>
                {
                    

                });

            }
            else if (e.PropertyName == NotifyChanges.Websocket_ObjectArgument)
            {
                var objectId = webSocketServiceAgent.ObjectArgument.Value.ToString();
                if (!localConfig.Globals.is_GUID(objectId)) return;
                if (!IsSuccessful_Login) return;
                if (!IsToggled_Listen) return;


            }



        }

        private void WebSocketServiceAgent_comServerOpened()
        {
            var authenticationRequest = new InterServiceMessage
            {
                ChannelId = Channels.Login,
                SenderId = webSocketServiceAgent.EndpointId
            };

            webSocketServiceAgent.SendInterModMessage(authenticationRequest);

        }

        private void WebSocketServiceAgent_comServerOnMessage(OntoMsg_Module.Notifications.InterServiceMessage message)
        {

            if (message.ReceiverId != null && message.ReceiverId != webSocketServiceAgent.EndpointId) return;

            if (!IsSuccessful_Login) return;
            var oItemMessage = message.OItems.LastOrDefault();
            if (oItemMessage == null) return;
            if (message.ChannelId == Channels.ParameterList && IsToggled_Listen)
            {
               
            }
            

        }

        private void CheckSendOrParamObject(string idItem)
        {
            
            var paramObject = serviceAgentElastic.GetOItem(idItem, localConfig.Globals.Type_Object);
            
            if (paramObject == null || oItemProcess.GUID_Parent != localConfig.OItem_type_process.GUID) return;

            oItemProcess = paramObject;
            

            Text_View = oItemProcess.Name;

        }

        public List<ViewModelProperty> GetViewModelProperties(bool onlySend = true, ViewItemType viewItemType = ViewItemType.All, ViewItemClass viewItemClass = ViewItemClass.All)
        {
            return ViewModelProperties.Where(viewItemProp => (onlySend ? viewItemProp.ViewModelAttribute.Send : 1 == 1)
                && (viewItemType != ViewItemType.All ? viewItemProp.ViewModelAttribute.ViewItemType == viewItemType : 1 == 1)
                && (viewItemClass != ViewItemClass.All ? viewItemProp.ViewModelAttribute.ViewItemClass == viewItemClass : 1 == 1)).ToList();
        }
    }
}
